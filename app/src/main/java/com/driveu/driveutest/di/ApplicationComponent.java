package com.driveu.driveutest.di;


import android.app.Application;
import android.content.Context;

import com.driveu.driveutest.DriveuApplication;
import com.driveu.driveutest.services.PollingService;
import com.driveu.driveutest.ui.map.MapViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(DriveuApplication driveuApplication);

    @ApplicationContext
    Context getContext();

    Application getApplication();

    void inject(PollingService pollingService);
    void inject(MapViewModel mapViewModel);
}
