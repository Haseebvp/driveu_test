package com.driveu.driveutest.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.driveu.driveutest.BuildConfig;
import com.driveu.driveutest.data.ApiFactory;
import com.driveu.driveutest.utils.AppConstants;
import com.driveu.driveutest.utils.SessionHandler;
import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApplicationModule {
    private final Application application;

    public ApplicationModule(Application app) {
        application = app;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreference() {
        return application.getSharedPreferences(AppConstants.SharedPrefsConstants.PREF_FILENAME, Context.MODE_PRIVATE);
    }

    @Provides
    @Inject
    @Singleton
    SessionHandler provideSessionHandler(SharedPreferences preferences) {
        return new SessionHandler(preferences);
    }

    @Provides
    @Inject
    @Singleton
    Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Inject
    @Singleton
    ApiFactory provideApiFactory(@ApplicationContext Context context, Retrofit retrofit) {
        return new ApiFactory(context, retrofit);
    }

    @Provides
    @Inject
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(AppConstants.BASE_URL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Inject
    @Singleton
    OkHttpClient provideOkHttpClient() {
        return BuildConfig.DEBUG ? getOkhttpClientDev(30) :
                getOkhttpClientProd(30);
    }

    private OkHttpClient getOkhttpClientDev(Integer timeout) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        return builder.build();
    }

    private OkHttpClient getOkhttpClientProd(Integer timeout) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .dispatcher(new Dispatcher());
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);
        }
        return builder.build();
    }

}
