package com.driveu.driveutest.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.arch.lifecycle.LifecycleService;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;

import com.driveu.driveutest.DriveuApplication;
import com.driveu.driveutest.R;
import com.driveu.driveutest.ui.map.MapViewModel;
import com.driveu.driveutest.ui.map.MapsActivity;
import com.driveu.driveutest.utils.SessionHandler;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class PollingService extends LifecycleService {

    private static final int ONGOING_NOTIFICATION_ID = 1001;

    private Disposable polling;
    private PowerManager.WakeLock mWakelock;
    private MapViewModel viewModel;

    @Inject
    SessionHandler sessionHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        DriveuApplication.get(this).getComponent().inject(this);
        viewModel = new MapViewModel(getApplication());
        sessionHandler.setIsPolling(true);
        init();
    }

    private void init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent notificationIntent = new Intent(this, MapsActivity.class);
            PendingIntent pendingIntent =
                    PendingIntent.getActivity(this, 0, notificationIntent, 0);
            createNotificationChannel();
            Notification notification =
                    new Notification.Builder(this, getString(R.string.default_channel_id))
                            .setContentTitle(getText(R.string.app_name))
                            .setContentText(getText(R.string.notification_message))
//                            .setSmallIcon(R.drawable.car)
                            .setContentIntent(pendingIntent)
                            .setTicker(getText(R.string.ticker_text))
                            .build();

            startForeground(ONGOING_NOTIFICATION_ID, notification);
        } else {
            try {
                PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                mWakelock = Objects.requireNonNull(powerManager).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DriveuWakeLock");
                mWakelock.acquire();
            } catch (Exception ignored) {

            }
        }
    }

    private void createNotificationChannel() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager.getNotificationChannel(getString(R.string.default_channel_id)) != null) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel defaultChannel = new NotificationChannel(getString(R.string.default_channel_id), getString(R.string.default_channel_name), importance);
                defaultChannel.enableLights(false);
                defaultChannel.setLightColor(Color.YELLOW);
                defaultChannel.enableVibration(false);
                defaultChannel.setShowBadge(false);
                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(defaultChannel);
                }
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startPolling();
        return Service.START_STICKY;
    }

    private void startPolling() {
        if (polling != null) {
            polling.dispose();
            polling = null;
        }
        polling = Observable
                .interval(15, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.computation())
                .toFlowable(BackpressureStrategy.DROP)
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap((Function<Long, Flowable<?>>) Flowable::just)
                .subscribe(aLong -> {
                    viewModel.getUpdatedLocation();
                });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy(){
        sessionHandler.setIsPolling(false);
        super.onDestroy();
    }

}
