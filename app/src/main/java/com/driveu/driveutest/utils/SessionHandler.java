package com.driveu.driveutest.utils;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class SessionHandler {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    public SessionHandler(SharedPreferences preference) {
        this.pref = preference;
        editor = pref.edit();
        editor.apply();
    }

    public void setIsPolling(boolean val) {
        editor.putBoolean(AppConstants.SharedPrefsConstants.IS_POLLING, val);
        editor.commit();
    }

    public LiveData<Boolean> isPolling() {
        MutableLiveData<Boolean> data = new MutableLiveData<>();
        Boolean out = pref.getBoolean(AppConstants.SharedPrefsConstants.IS_POLLING, false);
        data.setValue(out);
        return data;
    }
}
