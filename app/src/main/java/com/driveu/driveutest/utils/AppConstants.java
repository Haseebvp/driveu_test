package com.driveu.driveutest.utils;

public class AppConstants {
    public static final String BASE_URL = "";

    public class SharedPrefsConstants {
        public static final String PREF_FILENAME = "driveu_preference";
        public static final String IS_POLLING = "isPolling";
    }
}
