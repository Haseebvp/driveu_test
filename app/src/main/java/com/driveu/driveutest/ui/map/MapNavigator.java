package com.driveu.driveutest.ui.map;

public interface MapNavigator {
    void startPolling();
    void stopPolling();
}
