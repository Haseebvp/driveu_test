package com.driveu.driveutest.ui.map;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import com.driveu.driveutest.DriveuApplication;
import com.driveu.driveutest.data.ApiFactory;
import com.driveu.driveutest.ui.base.BaseViewModel;
import com.driveu.driveutest.utils.SessionHandler;
import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;

public class MapViewModel extends BaseViewModel<MapNavigator> {

    private LiveData<Boolean> pollingStatus;
    private LiveData<LatLng> locationUpdate;
    @Inject
    SessionHandler sessionHandler;
    @Inject
    ApiFactory apiFactory;

    public MapViewModel(Application application) {
        super(application);
        DriveuApplication.get(application).getComponent().inject(this);
        pollingStatus = sessionHandler.isPolling();
    }

    public boolean isPollingStarted() {
        return pollingStatus.getValue();
    }

    public void getUpdatedLocation() {
        locationUpdate = apiFactory.getUpdatedLocation();
    }

    public LiveData<LatLng> getLocationObservable() {
        return locationUpdate;
    }

}
