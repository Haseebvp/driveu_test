package com.driveu.driveutest.ui.map;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.driveu.driveutest.R;
import com.driveu.driveutest.databinding.ActivityMapsBinding;
import com.driveu.driveutest.services.PollingService;
import com.driveu.driveutest.ui.base.BaseActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends BaseActivity<ActivityMapsBinding, MapViewModel> implements OnMapReadyCallback, MapNavigator {

    private GoogleMap mMap;
    ActivityMapsBinding binding;
    MapViewModel viewModel;
    private Intent pollingService;
    private final static int LOCATION_REQUEST_CODE = 101;
    SupportMapFragment mapFragment;

    @Override
    public int getBindingVariable() {
        return BR.mapviewmodel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_maps;
    }

    @Override
    public MapViewModel getViewModel() {
        return viewModel;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_REQUEST_CODE);
            } else {
                mapFragment.getMapAsync(this);
            }
        } else {
            mapFragment.getMapAsync(this);
        }
        viewModel = new MapViewModel(getApplication());
        viewModel.setNavigator(this);
        pollingService = new Intent(this, PollingService.class);
        setSupportActionBar(binding.toolbar);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mapFragment.getMapAsync(this);
                } else {
                    Toast.makeText(this, R.string.permission_needed, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }

    }

    @Override
    public void startPolling() {
        startService(pollingService);
        observeViewModel();
    }

    @Override
    public void stopPolling() {
        startService(pollingService);
    }

    private void observeViewModel() {
        // Update the list when the data changes
        viewModel.getLocationObservable().observe(this, new Observer<LatLng>() {
            @Override
            public void onChanged(@Nullable LatLng latLng) {
                if (latLng != null) {
                    updateMarker(latLng);
                }
            }
        });
    }

    private void updateMarker(LatLng latLng) {
        if (mMap != null) {
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

}
