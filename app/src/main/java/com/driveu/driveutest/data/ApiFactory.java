package com.driveu.driveutest.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ApiFactory {
    private Retrofit retrofit;
    private Context context;

    public ApiFactory(Context context, Retrofit retrofit) {
        this.context = context;
        this.retrofit = retrofit;
    }

    public Retrofit retrofitBuilder() {
        return retrofit;
    }

    public ApiInterface getApiInterface() {
        return retrofit.create(ApiInterface.class);
    }

    public LiveData<LatLng> getUpdatedLocation() {
        final MutableLiveData<LatLng> data = new MutableLiveData<>();

        getApiInterface().getLocation().enqueue(new Callback<LatLng>() {
            @Override
            public void onResponse(Call<LatLng> call, Response<LatLng> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<LatLng> call, Throwable t) {
                data.setValue(null);
            }

        });

        return data;
    }

}
