package com.driveu.driveutest.data;

import com.google.android.gms.maps.model.LatLng;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("fetchlocation")
    Call<LatLng> getLocation();
}
