package com.driveu.driveutest;

import android.app.Application;
import android.content.Context;

import com.driveu.driveutest.di.ApplicationComponent;
import com.driveu.driveutest.di.ApplicationModule;
import com.driveu.driveutest.di.DaggerApplicationComponent;

public class DriveuApplication extends Application {
    protected ApplicationComponent applicationComponent;

    public static DriveuApplication get(Context context) {
        return (DriveuApplication) context.getApplicationContext();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }
}
